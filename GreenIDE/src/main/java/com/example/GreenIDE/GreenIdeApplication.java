package com.example.GreenIDE;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
public class GreenIdeApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreenIdeApplication.class, args);
	}



}
