package com.example.GreenIDE.configuration;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConfiService {
    public List<Confi> getConfis(){
        return List.of(
                new Confi("Compression",
                        0.0921,
                        0.0720,
                        1.2,
                        0.82));
    }
}
