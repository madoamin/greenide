package com.example.GreenIDE.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController

public class ConfiController {

    private final ConfiService confiService;

    @Autowired
    public ConfiController(ConfiService confiService) {
        this.confiService = confiService;
    }

    @GetMapping
    public List<Confi> getConfiguration (){
        return confiService.getConfis();
    }
}
