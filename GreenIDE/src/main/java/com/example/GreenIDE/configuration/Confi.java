package com.example.GreenIDE.configuration;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Confi {
    private String name;
    private double powerUsageBefor;
    private double powerUsageAfter;
    private double timeToBeDoneBefor;
    private double timeToBeDoneAfter;


    public Confi(String name, double powerUsageBefor,double powerUsageAfter, double timeToBeDoneBefor, double timeToBeDoneAfter) {
        this.name = name;
        this.powerUsageBefor = powerUsageBefor;
        this.powerUsageAfter = powerUsageAfter;
        this.timeToBeDoneBefor = timeToBeDoneBefor;
        this.timeToBeDoneAfter = timeToBeDoneAfter;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "name='" + name + '\'' +
                ", powerUsageBefor=" + powerUsageBefor +
                ", powerUsageAfter=" + powerUsageAfter +
                ", timeToBeDoneBefor='" + timeToBeDoneBefor + '\'' +
                ", timeToBeDoneAfter='" + timeToBeDoneAfter + '\'' +
                '}';
    }

}
